#!/usr/bin/env python3

from base64 import b64encode

NUM_USERS = 1000
HOST = "https://hourglass.home.kylesferrazza.com"

EXAM_ID = "1"
B64_EXAM_ID = b64encode(f"Exam-{EXAM_ID}".encode()).decode()

SOCKET_URI = "wss://hourglass.home.kylesferrazza.com/cable"
