#!/bin/sh
#!/bin/sh
POD_NAME="$(kubectl -n locust get pod -l component=master -o jsonpath='{.items[0].metadata.name}')"
kubectl -n locust logs -f $POD_NAME
