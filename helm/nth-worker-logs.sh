#!/bin/sh
set -eu

N="$1"
POD_NAME="$(kubectl -n locust get pod -l component=worker -o jsonpath="{.items[$N].metadata.name}")"
kubectl -n locust logs -f $POD_NAME
