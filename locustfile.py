#!/usr/bin/env python3

""" Hourglass load-testing with locust. """

from base64 import b64encode
from random import randint
from typing import Dict
from gevent import sleep
from locust import HttpUser, between, task, events
import locust.runners
from locust.runners import MasterRunner, WorkerRunner, LocalRunner
from bs4 import BeautifulSoup

from actioncable.connection import Connection as CableConnection
from actioncable.subscription import Subscription as CableSubscription
from actioncable.message import Message as CableMessage

locust.runners.HEARTBEAT_INTERVAL = 5
locust.runners.HEARTBEAT_LIVENESS = 30

HOST = "http://lb.home.kylesferrazza.com"
EXAM_ID = "1"
B64_EXAM_ID = b64encode(f"Exam-{EXAM_ID}".encode()).decode()
SOCKET_URI = "ws://lb.home.kylesferrazza.com/cable"
EXAM_TAKE_URL = f"/api/student/exams/{EXAM_ID}/take"

USERNAMES = []

# from: https://github.com/locustio/locust/blob/691f94c207ad4698b3081e7de8fe134a11c21b62/examples/custom_messages.py#L26-L47
@events.test_start.add_listener
def on_test_start(environment, **_kwargs):
    """
        When the test is started, evenly divides list between
        worker nodes to ensure unique data across threads.
    """
    global USERNAMES
    if not isinstance(environment.runner, WorkerRunner):
        USERNAMES = []
        for i in range(1000):
            user = f"stresstest{i}"
            reg_id = f"Registration-{i+2}" # 1 regular student, and IDs count from 1
            reg_id_encoded = b64encode(reg_id.encode('utf-8'))
            USERNAMES.append((user, reg_id_encoded.decode()))

def add_user(environment, msg, **_kwargs):
    """ Fired when the worker receives a message of type 'add_user' """
    USERNAMES.append(msg.data)
    print(f"Got user {msg.data[0]}")

def no_users_left(environment, msg, **_kwargs):
    """ Fired when the worker receives a message of type 'no_users_left' """
    print(f"No users left for me :(")
    USERNAMES.append(None)

def send_user_to_worker(environment, msg, **_kwargs):
    """ Fired when the master receives a message of type 'needs_user' """
    if len(USERNAMES) == 0:
        if isinstance(environment.runner, LocalRunner):
            environment.runner.send_message("no_users_left", {})
        else:
            environment.runner.send_message("no_users_left", {}, msg.node_id)
        return
    user = USERNAMES.pop()
    if isinstance(environment.runner, LocalRunner):
        environment.runner.send_message("add_user", user)
    else:
        environment.runner.send_message("add_user", user, msg.node_id)

@events.init.add_listener
def on_locust_init(environment, **_kwargs):
    """ Sets up event listeners """
    if not isinstance(environment.runner, MasterRunner):
        environment.runner.register_message("add_user", add_user)
        environment.runner.register_message("no_users_left", no_users_left)
    if not isinstance(environment.runner, WorkerRunner):
        environment.runner.register_message("needs_user", send_user_to_worker)

def block_for_creds(environment):
    print("Requested user.")
    environment.runner.send_message("needs_user")
    while len(USERNAMES) == 0:
        print("Still waiting for user.")
        sleep(2)
    return USERNAMES.pop()

SUBSCRIPTIONS = {
    "ExamMessages": {
        "name": "ExamMessagesSubscription",
        "query": "subscription ExamMessagesSubscription(\n  $registrationId: ID!\n) {\n  messageReceived(registrationId: $registrationId) {\n    message {\n      id\n      createdAt\n      body\n    }\n    messagesEdge {\n      node {\n        id\n      }\n    }\n  }\n}\n",
    },
    "ExamAnnouncements": {
        "name": "ExamMessagesNewAnnouncementSubscription",
        "query": "subscription ExamMessagesNewAnnouncementSubscription(\n  $examId: ID!\n) {\n  examAnnouncementWasSent(examId: $examId) {\n    examAnnouncement {\n      id\n      createdAt\n      body\n    }\n    examAnnouncementsEdge {\n      node {\n        id\n      }\n    }\n  }\n}\n",
    },
}

class GraphqlSubscription():
    def __init__(self, cable: CableConnection, identifier: str, variables: Dict[str, str], subscription_data: Dict[str, Dict[str, str]], on_rcv):
        self.identifier = identifier
        self.variables = variables
        self.subscription_data = subscription_data
        self.on_rcv = on_rcv
        self.actioncable_channel = CableSubscription(cable, self.identifier)
        self.actioncable_channel.on_receive(callback=on_rcv)
        data = {
            "variables": self.variables,
            "operationName": self.subscription_data["name"],
            "query": self.subscription_data["query"],
            # "action": "send",
        }
        self.actioncable_channel.create()
        msg = CableMessage("execute", data)
        self.actioncable_channel.send(msg)

class HourglassUser(HttpUser):
    """ Simulates an Hourglass user. """

    wait_time = between(2, 5)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.csrf_token = ""
        res = block_for_creds(self.environment)
        if not res:
            self.stop()
            return
        (username, reg_id) = res
        self.username = username
        self.password = username
        self.reg_id = reg_id
        self.snapshot_data = ''
        self.cable = None
        self.exam_messages_subscription = None
        self.exam_announcements_subscription = None

    def on_start(self):
        super().on_start()
        self.login()
        sleep(randint(1, 5))
        self.csrf_token = self.begin_exam()
        sleep(randint(1, 10))
        self.open_socket()
        self.setup_exam_messages_sub()
        self.setup_exam_announcements_sub()

    def log(self, msg: str):
        """ Logs a message with the current user's name """
        print(f"[{self.username}]\t{msg}")

    def login(self):
        """ Logs in using the next set of stress-testing credentials. """
        try_num = 0
        while True:
            try:
                try_num += 1
                self.log(f"Trying login (attempt #{try_num})...")
                res = self.client.get("/users/sign_in")
                self.log(f"CSRF GET status code: {res.status_code}")
                res.raise_for_status()
                soup = BeautifulSoup(res.text, features="html.parser")
                auth_token_elem = soup.find("meta", {"name":"csrf-token"})
                auth_token = auth_token_elem["content"]

                form_data = {
                    "user[username]": self.username,
                    "user[password]": self.password,
                    "commit": "Log in",
                    "authenticity_token": auth_token,
                }
                res = self.client.post("/users/sign_in", data=form_data)
                self.log(f"Login POST status code: {res.status_code}")
                res.raise_for_status()
                break
            except Exception as exc:
                self.log(f"Something bad happened while logging in: {str(exc)}")
                sleep(randint(1, 5))

    def begin_exam(self):
        """ Start the demo exam. """
        res = self.client.get("/?purpose=csrf")
        soup = BeautifulSoup(res.text, features="html.parser")
        auth_token_elem = soup.find("meta", {"name":"csrf-token"})
        auth_token = auth_token_elem["content"]
        body_dict = { "task": "start" }
        self.client.post(EXAM_TAKE_URL, json=body_dict, headers={"X-CSRF-TOKEN": auth_token})
        return auth_token

    @task
    def send_snapshot(self):
        """ Send answer snapshots. """
        self.snapshot_data += " more"
        data = {
            "task": "snapshot",
            "answers": {
                "answers":[
                    [
                        [{"NO_ANS":True},{"NO_ANS":True},{"NO_ANS":True},{"NO_ANS":True},{"NO_ANS":True},{"NO_ANS":True},{"NO_ANS":True},{"NO_ANS":True}],[{"NO_ANS":True},{"NO_ANS":True},{"NO_ANS":True},{"NO_ANS":True}]],[[{"NO_ANS":True},{"NO_ANS":True},{"text":self.snapshot_data,"marks":[]}],
                        [{"NO_ANS":True}],[{"NO_ANS":True},{"NO_ANS":True}]
                    ]
                ],
                "scratch":"stresstest2: 1"
            }
        }
        self.client.post(EXAM_TAKE_URL, headers={"X-CSRF-TOKEN": self.csrf_token}, json=data)
        self.log("Sent snapshot")

    def handle_new_sub_msg(self, msg):
        """ Handles new message from a subscription. """
        self.log(f"New subscription message: {msg}")

    def gen_subscription_identifier(self, purpose: str):
        channel_id = f"{self.username}_{purpose}"
        return {
            "channel": "GraphqlChannel",
            "channelId": channel_id,
        }

    def open_socket(self):
        """ Opens websocket connection on self.socket for messages. """
        self.log("Setting up websocket..")
        cookie_string = "; ".join([str(x)+"="+str(y) for x,y in self.client.cookies.items()])
        self.cable = CableConnection(url=SOCKET_URI, origin=HOST, cookie=cookie_string)
        self.cable.connect()
        sleep(1)
        while not self.cable.connected:
            sleep(1)
            self.log("Retrying cable connection...")
            self.cable.connect()

    def setup_exam_messages_sub(self):
        self.log(f"Setting up exam messages subscription with reg_id {self.reg_id}")

        identifier = self.gen_subscription_identifier("exam_messages")

        variables = {"registrationId": self.reg_id}
        self.exam_messages_subscription = GraphqlSubscription(self.cable, identifier, variables, SUBSCRIPTIONS["ExamMessages"], self.handle_new_sub_msg)

    def setup_exam_announcements_sub(self):
        self.log("Setting up exam announcements subscription")

        variables = {"examId": b64encode(b"Exam-1").decode()}
        identifier = self.gen_subscription_identifier("exam_announcements")
        self.exam_announcements_subscription = GraphqlSubscription(self.cable, identifier, variables, SUBSCRIPTIONS["ExamAnnouncements"], self.handle_new_sub_msg)

    @task
    def ask_question(self):
        """ Ask a professor questions. """
        self.log("Would ask question here...")

    def on_stop(self):
        super().on_stop()
        self.log("User stopped.")
        if self.cable:
            if self.cable.connected:
                self.cable.disconnect()
                self.log("Disconnected cable.")
        else:
            self.log("Cable was None when test stopped")
